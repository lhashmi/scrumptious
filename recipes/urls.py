from django.urls import path
from .views import show_recipes


urlpatterns = [
    path("1/", show_recipes)
]
