from django.shortcuts import render

def show_recipes(request):
    return render(request, "recipes/detail.html")
